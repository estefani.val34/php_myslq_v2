<?php

/**
 * Description of Product
 *
 * @author tarda
 */
class Product {

    private $id;
    private $name;
    private $description;
    private $category;
    private $price;

    public function __construct($id = NULL, $name = NULL, $description = NULL, $category = NULL, $price = NULL) {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->category = $category;
        $this->price = $price;
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getCategory() {
        return $this->category;
    }

    public function getPrice() {
        return $this->price;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setCategory($category) {
        $this->category = $category;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public function __toString() {
        return sprintf("%s;%s;%s;%s;%s\n", $this->id, $this->name, $this->description, $this->category, $this->price);
    }

}
