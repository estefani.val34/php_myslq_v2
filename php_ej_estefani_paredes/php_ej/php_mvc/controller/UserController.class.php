<?php

require_once "controller/ControllerInterface.php";
require_once "view/UserView.class.php";
require_once "model/UserModel.class.php";
require_once "model/User.class.php";
require_once "util/UserMessage.class.php";
require_once "util/UserFormValidation.class.php";

class UserController implements ControllerInterface {

    private $view;
    private $model;

    public function __construct() {
        // carga la vista
        $this->view = new UserView();
        
        // carga el modelo de datos
        $this->model = new UserModel();
    }

    // carga la vista según la opción o ejecuta una acción específica
    public function processRequest() {

        $request = NULL;
        $_SESSION['info'] = array();
        $_SESSION['error'] = array();

        // recupera la acción de un formulario
        if (filter_has_var(INPUT_POST, 'action')) {
            $request = filter_has_var(INPUT_POST, 'action') ? filter_input(INPUT_POST, 'action') : NULL;
        }
        // recupera la opción de un menú
        else {
            $request = filter_has_var(INPUT_GET, 'option') ? filter_input(INPUT_GET, 'option') : NULL;
        }

        switch ($request) {//lñegan las acciones de menu y los botones 
            case "form_add": 
                $this->formAdd();
                break;
            case "add"://cuando haga click al boton add
                $this->add();
                break;
            case "list_all":
                $this->listAll();
                break;
            case "form_search":// serach modify i dellete 
                $this->formSearch();
                break;
            case "search":// serach modify i dellete 
                $this->searchById();
                break;
             case "form_modify":// serach modify i dellete 
                $this->formModify();
                break;
            case "modify":// serach modify i dellete 
                $this->modify();
                break;
            case "delete":// serach modify i dellete 
                $this->delete();
                break;
            default:
                $this->view->display();
        }
    }

    // ejecuta la acción de mostrar todas las categorías
    public function listAll() {
        $users = $this->model->listAll();

        if (!empty($users)) { 
            $_SESSION['info'] = UserMessage::INF_FORM['found'];
        } else {
            $_SESSION['error'] = UserMessage::ERR_FORM['not_found'];
        }

        $this->view->display("view/form/UserList.php", $users);
    }

    // carga el formulario de insertar categoría
    public function formAdd() {
        $roles = $this->model->listRoles();
        $this->view->display("view/form/UserFormAdd.php", null, $roles);
       
    }

    // ejecuta la acción de insertar categoría
    public function add() {
        $userValid = UserFormValidation::checkData(UserFormValidation::ADD_FIELDS);
        if (empty($_SESSION['error'])) {
            $user = $this->model->searchById($userValid->getUsername());

            if (is_null($user)) {
                $result = $this->model->add($userValid);

                if ($result == TRUE) {
                    $_SESSION['info'] = UserMessage::INF_FORM['insert'];
                    $userValid = NULL;
                }
            } else {
                $_SESSION['error'] = UserMessage::ERR_FORM['exists_username'];
            }
        }

        $roles = $this->model->listRoles();
        $this->view->display("view/form/UserFormAdd.php", $userValid, $roles);
        
    }
    
    // carga el formulario de modificar categoria
    public function formSearch() {
        $roles = $this->model->listRoles();
        $this->view->display("view/form/UserFormSearch.php", null, $roles);
    }
    // ejecuta la acción de buscar categoría por id de categoría A PUESTO ALGODE ESTO EN GITLAB 
    public function searchById() {
        $userValid = UserFormValidation::checkData(UserFormValidation::SEARCH_FIELDS);
         //print_r($userValid);
        if (empty($_SESSION['error'])) {
            $user = $this->model->searchById($userValid->getUsername());

            if (!is_null($user)) { // is NULL or user object?
                $_SESSION['info'] = UserMessage::INF_FORM['found'];
                $userValid = $user;
            } else {
                $_SESSION['error'] = UserMessage::ERR_FORM['not_found'];
            }
        }
        
          if (!empty($_SESSION['error'])) {
             $roles = $this->model->listRoles();
             $this->view->display("view/form/UserFormSearch.php", $userValid, $roles);  
          }else{
              $this->formModify($userValid) ;
          }
       
        //hacer un if; si existe modificar, si no nada
    }

    
    // carga el formulario de modificar categoria
    public function formModify($userValid) {
         $roles = $this->model->listRoles();
         $this->view->display("view/form/UserFormModify.php", $userValid, $roles);
    }

    // ejecuta la acción de modificar categoría    
    public function modify() {
         $userValid = UserFormValidation::checkData(UserFormValidation::MODIFY_FIELDS);

        if (empty($_SESSION['error'])) {
            $user = $this->model->searchById($userValid->getUsername());

            if (!is_null($user)) {
                $result = $this->model->modify($userValid);

                if ($result == TRUE) {
                    $_SESSION['info'] = UserMessage::INF_FORM['update'];
                    $userValid = NULL;
                }
            } else {
                $_SESSION['error'] = UserMessage::ERR_FORM['not_exists_username'];
            }
        }
        
      $roles = $this->model->listRoles();
      $this->view->display("view/form/UserFormModify.php", $userValid, $roles);
      
    }

    // ejecuta la acción de eliminar categoría    
    public function delete() {
          $userValid = UserFormValidation::checkData(UserFormValidation::DELETE_FIELDS);

        if (empty($_SESSION['error'])) {
            $user = $this->model->searchById($userValid->getUsername());

            if (!is_null($user)) {
                $result = $this->model->delete($userValid->getUsername());

                if ($result == TRUE) {
                    $_SESSION['info'] = UserMessage::INF_FORM['delete'];
                    $userValid = NULL;//para que me muestre vacia la lista
                }
            } else {
                $_SESSION['error'] = UserMessage::ERR_FORM['not_exists_username'];
            }
        }
        
     $roles = $this->model->listRoles();
     $this->view->display("view/form/UserFormModify.php", $userValid, $roles);
        
    }

    

}
