<?php

require_once "model/persist/UserFileDAO.class.php";
require_once "model/ProductModel.class.php";

class UserModel {

    private $dataUser;

    public function __construct() {
        // File
        $this->dataUser = UserFileDAO::getInstance();

    }

    /**
     * select all users
     * @param void
     * @return array of users objects or array void
     */
    public function listAll(): array {
        $users = $this->dataUser->listAll();
        return $users;
    }

    /**
     * insert a user
     * @param user user object to insert
     * @return TRUE or FALSE
     */
    public function add($user): bool {
        $result = $this->dataUser->add($user);

        if ($result == FALSE) {
            $_SESSION['error'] = UserMessage::ERR_DAO['insert'];
        }
        return $result;
    }

    /**
     * select a user by Id
     * @param $id string user Id
     * @return user object or NULL
     */
    public function searchById($username) {
        $user = $this->dataUser->searchById($username);
        return $user;
    }

    /**
     * update a user
     * @param user user object to update
     * @return TRUE or FALSE
     */
    public function modify($user): bool {
        $result = $this->dataUser->modify($user);
        if ($result == FALSE ) {//false
            $_SESSION['error'] = UserMessage::ERR_DAO['update'];
        }
        return $result;
    }

    /**
     * delete a user . I can delete a user if this have products , this is in used
     * @param $username string user $username to delete
     * @return TRUE or FALSE
     * 
     */
    public function delete($username): bool {
        $result = $this->dataUser->delete($username);
        if (!$result) {
            $_SESSION['error'] = UserMessage::ERR_DAO['delete'];
        }
        return $result;
    }
    
    public function listRoles(): array {
        $result = array(
            'Basic' => 'Basic',
            'Advanced' => 'Advanced',
            );
        return $result;
    }
    
}
