<?php

require_once "model/ModelInterface.class.php";
require_once "model/persist/ConnectFile.class.php";

class UserFileDAO implements ModelInterface {

    private static $instance = NULL; // instancia de la clase
    private $connect; // conexión actual

    const FILE = "model/resource/users.txt";

    public function __construct() {
        $this->connect = new ConnectFile(self::FILE);
    }

    // singleton: patrón de diseño que crea una instancia única
    // para proporcionar un punto global de acceso y controlar
    // el acceso único a los recursos físicos
    public static function getInstance(): UserFileDAO {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * select all categories from file
     * @param void
     * @return array of user objects or array void
     */
    public function listAll(): array {
        $result = array();

        // abre el fichero en modo read
        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);
                    $user = new User($fields[0],$fields[1],$fields[2], $fields[3],$fields[4]);
                    //print_r($user);
                    array_push($result, $user);
                }
            }
            $this->connect->closeFile();
        }
       // print_r($result);
        return $result;
    }

    /**
     * insert a user in file
     * @param $user user object to insert
     * @return TRUE or FALSE
     */
    public function add($user): bool {
        $result = FALSE;

        // abre el fichero en modo append
        if ($this->connect->openFile("a+")) {
            fputs($this->connect->getHandle(), $user->__toString());
            $this->connect->closeFile();
            $result = TRUE;
        }

        return $result;
    }

    /**
     * select a user by username from file
     * @param username string user username
     * @return user object or NULL
     */
    public function searchById($username) {
        $user = NULL;
       
        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);

                    if ($username == $fields[0]) {
                        $user = new User($fields[0],$fields[1],$fields[2], $fields[3],$fields[4]);
                        break;
                    }
                }
            }
            $this->connect->closeFile();
        }
        return $user;
    }

    /**
     * update a user in file
     * @param user user object to update
     * @return TRUE or FALSE
     */
    public function modify($user): bool {
        $result = FALSE;
        $fileData = array();

        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);

                    if ($user->getUsername() == $fields[0]) { 
                        array_push($fileData, $user->__toString()); 
                    } else {
                        array_push($fileData, $line . "\n");
                    }
                }
            }
            $this->connect->closeFile();
        }
        if ($this->connect->writeFile($fileData)) {
            $result = TRUE;
        }
        return $result;
    }

    /**
     * delete a user in file
     * @param $username string user $username to delete
     * @return TRUE or FALSE
     */
    public function delete($username): bool {
        $result = FALSE;
        $fileData = array();

        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);

                    if ($username !== $fields[0]) {//guardo la line si es distinto 
                         array_push($fileData, $line . "\n");
                    } 
                }
            }
            $this->connect->closeFile();
        }
        if ($this->connect->writeFile($fileData)) {
            $result = TRUE;
        }
        return $result;
    }

}
