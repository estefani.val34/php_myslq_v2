<div id="content">
    <form method="post" action="">
        <fieldset>
            <legend>Modify employee</legend>
            <label>Id *:</label>
            <input type="text" placeholder="Id" name="id" value="<?php if (isset($content)) { echo $content->getId(); } ?>" />
            <label>Name *:</label>
            <input type="text" placeholder="Name" name="name" value="<?php if (isset($content)) { echo $content->getName(); } ?>" />
            <label>Surname :</label>
            <textarea  name="surname" placeholder="Surname" rows="10" cols="30" style="width: 236px; height: 40px; margin-left: 17px; border-radius: 4px;"><?php if (isset($content)) { echo $content->getSurname(); } ?></textarea>
            <label>Category :</label>
                    <?php  
                           foreach($categories as $category){
                               
                                 if (isset($content) && ($content->getCategory() == $category->getId())) {
                                     echo ' <input type="radio" name="category" value="'.$category->getId().'" checked />'.$category->getName();
                                 }else{
                                     echo ' <input type="radio" name="category" value="'.$category->getId().'"/>'.$category->getName();
                                 }
                           }  
                    ?>
       
            <label>* Required fields</label>
            <input type="submit" name="action" value="search" />
            <input type="submit" name="action" value="modify" />
            <input type="submit" name="action" value="delete" />
            <input type="submit" name="reset" value="reset" onClick="form_reset(this.form.id); return FALSE;" />
        </fieldset>
    </form>
</div>