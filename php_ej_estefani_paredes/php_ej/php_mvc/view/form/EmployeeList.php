<div id="content">
    <fieldset>
        <legend>Employee list</legend>    
        <?php
            if (isset($content)) {
                echo <<<EOT
                    <table>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Surname</th>
                            <th>Category</th>
                           
                        </tr>
EOT;
                foreach ($content as $employee) {
                    echo <<<EOT
                        <tr>
                            <td>{$employee->getId()}</td>
                            <td>{$employee->getName()}</td>
                            <td>{$employee->getSurname()}</td>
                            <td>{$employee->getCategory()}</td>
                            
                        </tr>
EOT;
                }
                echo <<<EOT
                    </table>
EOT;
            }
        ?>
    </fieldset>
</div>
