<?php

class UserFormValidation {

    const ADD_FIELDS = array('username', 'password', 'age', 'role', 'active');
    const MODIFY_FIELDS = array('username', 'password', 'age', 'role', 'active');
    const DELETE_FIELDS = array('username');
    const SEARCH_FIELDS = array('username');
    const NUMERIC = "/[^0-9]/";
    const ALPHABETIC = "/[^a-z A-Z|-]/";

    public static function checkData($fields) {
        $username = NULL;
        $password = NULL;
        $age = NULL;
        $role = NULL;
        $active = NULL;


        foreach ($fields as $field) {
            switch ($field) {
                case 'username':
                    // filter_var retorna los datos filtrados o FALSE si el filtro falla
                    $username = trim(filter_input(INPUT_POST, 'username'));
                    $usernameValid = !preg_match(self::ALPHABETIC, $username);
                    if (empty($username)) {
                        array_push($_SESSION['error'], UserMessage::ERR_FORM['empty_username']);
                    } else if ($usernameValid == FALSE) {
                        array_push($_SESSION['error'], UserMessage::ERR_FORM['invalid_username']);
                    }
                    break;
                case 'password':
                    $password = trim(filter_input(INPUT_POST, 'password'));
                    $passwordValid = !preg_match(self::ALPHABETIC, $password);
                    if (empty($password)) {
                        array_push($_SESSION['error'], UserMessage::ERR_FORM['empty_password']);
                    } else if ($passwordValid == FALSE) {
                        array_push($_SESSION['error'], UserMessage::ERR_FORM['invalid_password']);
                    }
                    break;
                case 'age':
                    $age = trim(filter_input(INPUT_POST, 'age'));
                    $ageValid = !preg_match(self::NUMERIC, $age);
                    if ($ageValid == FALSE) {
                        array_push($_SESSION['error'], UserMessage::ERR_FORM['invalid_age']);
                    }
                    break;

                case 'role':
                    $role = trim(filter_input(INPUT_POST, 'role'));
                    break;
                case 'active':
                    $active = trim(filter_input(INPUT_POST, 'active'));
                    $active="YES";
                    break;
            }
        }
       
       
        $user = new User($username,  $password, $age , $role, $active);
         //print_r($user);
        // print_r($user->getUsername());
        return $user;
    }

}
