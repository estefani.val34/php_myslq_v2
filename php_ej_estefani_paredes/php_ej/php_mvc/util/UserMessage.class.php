<?php

class UserMessage {

    const INF_FORM =
        array(
            'insert' => 'Data inserted successfully',
            'update' => 'Data updated successfully',
            'delete' => 'Data deleted successfully',
            'found'  => 'Data found',
            '' => ''
        );
    
    const ERR_FORM =
        array(
            'empty_username'      => 'username must be filled',
            'empty_password'    => 'password must be filled',
            'invalid_username'    => 'username must be valid values',
            'invalid_password'  => 'password must be valid values',
            'invalid_age'  => 'age must be valid values',
            'exists_username'     => 'username already exists',
            'not_exists_username' => 'username not exists',
            'not_found'     => 'No data found',
            '' => ''
        );

    const ERR_DAO =
        array(
            'insert' => 'Error inserting data',
            'update' => 'Error updating data',
            'delete' => 'Error deleting data',
            'used'   => 'No data deleted, Client in use',
            '' => ''
        );
    
    
}
